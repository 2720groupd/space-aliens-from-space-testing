#include <iostream>
#include <iomanip>

#include "Player.h"
using namespace std;

Player::Player(int h, int d)
{
  hp  = h;
  dmg = d;
}

void Player::getInventory()
{
    //Determines if the user has weapons and if so will list all weapons in their inventory
  if (hasWeapons)
    {
      cout << endl;
      cout << setw(12) << left << "Weapon Name";
      cout << setw(12) << right << "Damage" << endl;
      for (unsigned int i=0; i<weapons.size(); i++){
	cout << setw(12) << setfill('-') << left << weapons.at(i).getName();
	cout << setw(12) << setfill('-') << right << weapons.at(i).strength();
      }
    }
  else 
    cout << "You have no weapons." << endl;

  //Determines if the user has tools and if so will list all tolls in their inventory
  if (hasTools)
    {
        cout << endl << "The tools you have are ";
        for (unsigned int i=0; tools.size(); i++)
            cout << endl << tools.at(i).getName();
    }
  else 
    cout << "\nYou have no tools." << endl;

  //Checks to see if the user has any rocks if so will display how many they contain
  cout << "You have " << rocks << " rocks.";
}

//Equips weapon
void Player::equipWeapon(Weapon weapon)
{
	//If a weapon is equiped then subtract its damage from players total damage before switching weapons

  
      if (equipedWeapon!=NULL)
	 dmg = dmg-equipedWeapon->strength();
      equipedWeapon = &weapon;
      dmg = dmg+weapon.strength();
}

//Equips tool
void Player::equipTool(Tools tool)
{
   equipedTool = &tool;
}

//Destroys item
void Player::destroyItem()
{
    //TODO
}

void Player::addWeapon(Weapon weapon)
{
   weapons.push_back(weapon);
   hasWeapons = 1;

}

bool Player::checkWeapons()
{
   return hasWeapons;
}

Weapon Player::returnWeapon(int i)
{
   return weapons.at(i);
}
