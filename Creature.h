#ifndef __CREATURE_H_
#define __CREATURE_H_

///Creature class is used to represent all creatures in the came that can be played or fought.
class Creature
{
 protected:
  //  string name;
  int hp=100, dmg=0; //Health and Damage values
  //dmg number will change with player equipment, monsters have static values

  public:
  ///Default constructor, accepts no arguments.
  Creature();

  ///Constructor. Takes two integers as arguments to set the Hp and Dmg of the creature.
  Creature(int h, int d); 

  ///Accepts a pointer to a creature as a parameter that will be attacked by the player.
  void attack(Creature *target); //Attack <target>

  ///Accepts an integer as a parameter which is used to update the damage a player/ creature has taken.
  void takeDamage(int d); //Take hp damage

  //Setter and getter functions
  ///Takes an integer as argument to set the damage that a creature has.
  void setDmg(int d);

  ///Takes an integer as argument to set the Hp that a creature has.
  void setHp(int h);

  ///Returns the current Dmg value for a creature.
  int getDmg();

  ///Returns the current DmgHp value for a creature.
  int getHp();
};

#endif
