#ifndef __TOOLS_H_
#define __TOOLS_H_

#include<string>
using namespace std;

///Represents the tools used in the game.
class Tools
{
private:
	string name;	//Name of the tool

  public:
	///Constructor. Accepts a string as argument to give the tool a name.
	Tools(string n);
	
	///Returns the name of the tool.
	string getName();
};

#endif
