#ifndef __GAME_H_
#define __GAME_H_

#include "Player.h"
#include "Creature.h"

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
//istream& cin
using namespace std;

///Base class that all scenes in the game will inherit from.
class Game
{
  public:
   ///Pointer to the player of the game.
   Player *user;

   ///A vector that contains the options that the user may choose from.
   vector<string> options;

   ///Default constructor, accepts no arguments.
   Game();

   ///Calls the playScene() function to progress throught the game.
   void playGame();

   ///Sequentially progresses through each scene in the game. 
   void playScene(int sceneNumber=1);

   ///Collects input from the user to move the game forward.
   string getUserChoice();

    ///Takes the word and finds a suitable "alternate" word for streamlining (ex: use/throw/eat/attack : Will all be 'use')
   string generalizedWord(string);

   ///Is called when the player dies. Takes a scene number and a reaseon as argument to let the player know why.
   void gameOver(int sceneNumber, string reason="because of an unknown reason."); //Died on scene sceneNumber because reason

   ///Checks whether the game is over. Accepts no parameters.
   void isOver();

   ///Fighting NPCs return 0 for user death, return 1 if user won.
   bool battle(Creature *enemy);

};

#endif
