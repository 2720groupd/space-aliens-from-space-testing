#ifndef SCENE_H
#define SCENE_H


#include <vector>
#include <string>
#include "Game.h"
using namespace std;

///Each Scene_x class is derived from the Game Class.
class Scene_1 : public Game
{
  public:
///Contains the gameplay for each scene. Returns false if the user dies. Returns 1 if user completes it successfully.
   bool play(Player* gameUser);
};

class Scene_2 : public Game
{
  public:
   bool play(Player* gameUser);
};

class Scene_3 : public Game
{
  public:
   bool play(Player* gameUser);
};


class Scene_4 : public Game
{
  public:
   bool play(Player* gameUser);
};

class Scene_5 : public Game
{
  public:
   bool play(Player* gameUser);
};

class Scene_6 : public Game
{
  public:
   bool play(Player* gameUser);
};

class Scene_7: public Game
{
  public:
   bool play(Player* gameUser);
};

class Scene_8: public Game//player declines to help alien.
{
  public:
   bool play(Player* gameUser);
};

class Scene_9: public Game//continuation of player declines to help alien.
{
  public:
   bool play(Player* gameUser);
};

class Scene_10: public Game//Player agrees to help alien.
{
  public:
   bool play(Player* gameUser);
};

class Scene_11: public Game//Continuation of player agrees to help alien.
{
  public:
   bool play(Player* gameUser);
};

class Scene_12: public Game//Final scene.
{
  public:
   bool play(Player* gameUser);
};

///Represents the main menu for the game. Derived from game.
class MainMenu : public Game
{
  public:
   ///Default constructor. Accepts no arguments.
  MainMenu();
};

#endif
