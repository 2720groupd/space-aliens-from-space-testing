#ifndef __PLAYER_H_
#define __PLAYER_H_

#include <string>
#include <vector>
#include "Creature.h"
#include "Weapon.h"
#include "Tools.h"
using namespace std;

///Derives from the Creature class. Represents the user who is playing the game.
class Player : public Creature
{
  public:
    Weapon *equipedWeapon=NULL; //Contains players equiped Weapon
    Tools *equipedTool=NULL;    //Contains players equiped tool

    vector<Weapon> weapons; //Contains list of all weapons the user has
    vector<Tools> tools;    //Contains list of all tools the user has
    int rocks = 0;          //Will contain the number of rocks the user has
    
    bool hasTools=0; //Do I have <tools>
    bool hasWeapons=0; //Do I have <weapon>
    //bool hasRock=0; //etc...
    //This would all be one (bool hasTools, hasWeapon, hasRock, ... ;)
    ///Constructor. Takes two ints as argument to set the Hp and Dmg of the player.
    Player(int h=100, int d=10);

    ///Lists the players current inventory.
    void getInventory();

    ///Takes a weapon as argument and sets it to the weapon that is currently equipped. Will update the players Dmg.
    void equipWeapon(Weapon weapon); 

    ///Takes a tool as argument and updates the pointer to the current tool that is being used.
    void equipTool(Tools tool);

    ///Destroys an item after is is done being used.
    void destroyItem();

    ///Add's a weapon to weapons vector.
    void addWeapon(Weapon weapon);

    ///Checks hasWeapons data member.
    bool checkWeapons();

    ///return a weapon from the vector by index.
    Weapon returnWeapon(int);
    
    
};

#endif
