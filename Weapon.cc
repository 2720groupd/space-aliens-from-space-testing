#include <string>
#include "Weapon.h"
using namespace std;

//Creates a weapon with name=n and damage=dmg
Weapon::Weapon(string n, int dmg){
	name = n;
	wDmg = dmg;
}

//Returns the amount of damage the weapon deals
int Weapon::strength(){
	return wDmg;
}

//Returns the name of the weapon
string Weapon::getName(){
	return name;
}
