//**************************************************************************************
// TestTools Header File for SpaceAliensFromSpace Tests
//Author: Group D
//Group Assignment 3 CPSC 2720
//**************************************************************************************

#ifndef TESTTOOLS_H
#define TESTTOOLS_H

#include "../Tools.h"
#include<cppunit/extensions/HelperMacros.h>


class TestTools : public CppUnit::TestFixture
{
    //Macro for creating test suite
    CPPUNIT_TEST_SUITE( TestTools );
    CPPUNIT_TEST(getName_testTool_returntestRock);
    CPPUNIT_TEST_SUITE_END();
private:
    Tools *testTool;
public:
    void setUp();
    void tearDown();
    void getName_testTool_returntestRock();
};

#endif