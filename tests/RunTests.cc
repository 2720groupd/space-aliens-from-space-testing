//**************************************************************************************
// Test Runner for SpaceAliensFromSpace Tests
//Author: Group D
//Group Assignment 3 CPSC 2720
//**************************************************************************************

#include <cppunit/ui/text/TestRunner.h>
#include "TestTools.h"
#include "TestWeapon.h"
#include "TestPlayer.h"
#include "TestGame.h"
#include "TestCreature.h"

int main()
{
    //This file runs all the test cases passed into it
    
    CppUnit::TextUi::TestRunner runner;
    
    //Pass in of all class test suites
    runner.addTest(TestPlayer::suite());
    runner.addTest(TestCreature::suite());
    runner.addTest(TestTools::suite());
    runner.addTest(TestWeapon::suite());
 	runner.addTest(TestGame::suite());

 

    
    //Executes run of tests
    runner.run();
    
    return 0;
}
