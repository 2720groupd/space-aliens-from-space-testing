//**************************************************************************************
// TestWeapon Implementation File for SpaceAliensFromSpace Tests
//Author: Group D
//Group Assignment 3 CPSC 2720
//**************************************************************************************

#include "TestGame.h"
#include <string>
#include <iostream>
#include <sstream>
#include <string>
void TestGame::setUp()
{
	game = new Game();
	creature = new Creature(100, 10);
	//player = new Player();
	string str = "Quit game \n Quit game";
	istringstream sin(str);
}

void TestGame::tearDown()
{
	delete game;
	delete creature;
	//delete player;
}

void TestGame::TestGeneralizedUse()
{
	CPPUNIT_ASSERT(game->generalizedWord("fire ")=="use ");
}

void TestGame::TestGeneralizedExplore()
{
	CPPUNIT_ASSERT(game->generalizedWord("look ")=="explore ");
}

void TestGame::TestGeneralizedGet()
{
	CPPUNIT_ASSERT(game->generalizedWord("stand ")=="get ");
}

void TestGame::TestGeneralizedAttack()
{
	CPPUNIT_ASSERT(game->generalizedWord("fight ")=="attack ");
}

void TestGame::TestGeneralizedWrong()
{
	CPPUNIT_ASSERT(game->generalizedWord("swing sword ")!="attack ");
}

void TestGame::TestGameOver()
{
	CPPUNIT_ASSERT_NO_THROW(game->gameOver(0,"dead"));
}


