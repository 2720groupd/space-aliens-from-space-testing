//**************************************************************************************
// TestTools Implementation File for SpaceAliensFromSpace Tests
//Author: Group D
//Group Assignment 3 CPSC 2720
//**************************************************************************************

#include "TestTools.h"

void TestTools::setUp()
{
	testTool = new Tools("testRock");
}

void TestTools::tearDown()
{
	delete testTool;
}

void TestTools::getName_testTool_returntestRock()
{
	CPPUNIT_ASSERT(testTool->getName() == "testRock");
}