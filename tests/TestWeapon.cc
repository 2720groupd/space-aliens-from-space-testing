//**************************************************************************************
// TestWeapon Implementation File for SpaceAliensFromSpace Tests
//Author: Group D
//Group Assignment 3 CPSC 2720
//**************************************************************************************

#include "TestWeapon.h"

void TestWeapon::setUp()
{
	sword = new Weapon("SharpSword", 100);
}

void TestWeapon::tearDown()
{
	delete sword;
}

void TestWeapon::TestSwordName()
{
CPPUNIT_ASSERT(sword->getName()=="SharpSword");
}

void TestWeapon::TestSwordDam()
{
CPPUNIT_ASSERT(sword->strength()==100);
}
