//**************************************************************************************
// TestWeapon File for SpaceAliensFromSpace Tests
//Author: Group D
//Group Assignment 3 CPSC 2720
//**************************************************************************************

#ifndef TESTWEAPON_H
#define TESTWEAPON_H

#include "../Weapon.h"
#include <cppunit/extensions/HelperMacros.h>


class TestWeapon : public CppUnit::TestFixture
{
    //Macro for creating test suite
    CPPUNIT_TEST_SUITE( TestWeapon );
    CPPUNIT_TEST(TestSwordDam);
    CPPUNIT_TEST(TestSwordName);
    CPPUNIT_TEST_SUITE_END();
private:
	Weapon *sword; 

public:
    void setUp();
    void tearDown();
    void TestSwordDam();
    void TestSwordName();
};

#endif