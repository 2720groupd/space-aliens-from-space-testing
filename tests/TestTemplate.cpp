//**************************************************************************************
// Bishop Piece Unit Tests Implementation File
//Author: Matthew Meier
//Assignment 3 CPSC 2720
//**************************************************************************************

#include "../Board.h"
#include "../Square.h"
#include "../BishopMovement.h"
#include "../ChessBoard.h"
#include "TestBishop.h"

void TestBishop::setUp()
{
    mainBoardBishop = new ChessBoard(5,5);
    testBishop = new Piece(Piece::white, 'b', new BishopMovement());
}

void TestBishop::tearDown()
{
    delete mainBoardBishop;
    delete testBishop;
}

void TestBishop::movePiece_bishopMoveUpLeftDiagonalBoundary_pieceMoved()
{
    mainBoardBishop->placePiece(testBishop, mainBoardBishop->getSquare(2,2));
    mainBoardBishop->movePiece(mainBoardBishop->getSquare(2,2), mainBoardBishop->getSquare(0,0));
    CPPUNIT_ASSERT(mainBoardBishop->getSquare(0,0)->getPiece() == testBishop);
}

void TestBishop::movePiece_bishopMoveDownLeftDiagonalBoundary_pieceMoved()
{
    mainBoardBishop->placePiece(testBishop, mainBoardBishop->getSquare(2,2));
    mainBoardBishop->movePiece(mainBoardBishop->getSquare(2,2), mainBoardBishop->getSquare(4,0));
    CPPUNIT_ASSERT(mainBoardBishop->getSquare(4,0)->getPiece() == testBishop);
}

void TestBishop::movePiece_bishopMoveUpRightDiagonalBoundary_pieceMoved()
{
    mainBoardBishop->placePiece(testBishop, mainBoardBishop->getSquare(2,2));
    mainBoardBishop->movePiece(mainBoardBishop->getSquare(2,2), mainBoardBishop->getSquare(0,4));
    CPPUNIT_ASSERT(mainBoardBishop->getSquare(0,4)->getPiece() == testBishop);
}

void TestBishop::movePiece_bishopMoveDownRightDiagonalBoundary_pieceMoved()
{
    mainBoardBishop->placePiece(testBishop, mainBoardBishop->getSquare(2,2));
    mainBoardBishop->movePiece(mainBoardBishop->getSquare(2,2), mainBoardBishop->getSquare(4,4));
    CPPUNIT_ASSERT(mainBoardBishop->getSquare(4,4)->getPiece() == testBishop);
}

void TestBishop::movePiece_bishopMoveVertical_throwExcpetion()
{
    mainBoardBishop->placePiece(testBishop, mainBoardBishop->getSquare(2,2));
    mainBoardBishop->movePiece(mainBoardBishop->getSquare(2,2), mainBoardBishop->getSquare(1,2));
}

void TestBishop::movePiece_bishopMoveHorizontal_throwException()
{
    mainBoardBishop->placePiece(testBishop, mainBoardBishop->getSquare(2,2));
    mainBoardBishop->movePiece(mainBoardBishop->getSquare(2,2), mainBoardBishop->getSquare(2,3));
}