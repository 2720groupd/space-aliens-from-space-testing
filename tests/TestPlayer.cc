//**************************************************************************************
// TestPlayer Implementation File for SpaceAliensFromSpace Tests
//Author: Group D
//Group Assignment 3 CPSC 2720
//**************************************************************************************

#include "TestPlayer.h"

void TestPlayer::setUp()
{
	testPlayer = new Player(); //this is how the cosntructor is called in game
	testSword = new Weapon("Sword", 100);
	testMace = new Weapon("Mace", 50);
	testTool = new Tools("Hammer");
}

void TestPlayer::tearDown()
{
	delete testPlayer;
	delete testSword;
	delete testMace;
}



void TestPlayer::addWeapon_addTestSword_swordInInventory()
{
	testPlayer->addWeapon(*testSword);
	CPPUNIT_ASSERT(testPlayer->weapons[0].getName() == testSword->getName());
}

void TestPlayer::checkWeapons_noWeaponPresent_returnFalse()
{
	CPPUNIT_ASSERT(testPlayer->checkWeapons() == false);
}

void TestPlayer::checkWeapons_weaponInInventory_returnTrue()
{
	testPlayer->addWeapon(*testSword);
	CPPUNIT_ASSERT(testPlayer->checkWeapons() == true);
}

void TestPlayer::returnWeapon_swordInInventory_returnSword()
{
	testPlayer->addWeapon(*testSword);
	CPPUNIT_ASSERT(testPlayer->returnWeapon(0).getName() == testSword->getName());
}


void TestPlayer::equipWeapon_equipSword_damageIncreased()
{
	testPlayer->addWeapon(*testSword);
	testPlayer->equipWeapon(*testSword);
	CPPUNIT_ASSERT(testPlayer->getDmg() == 110);
}

void TestPlayer::equipWeapon_equipMaceOverSword_DamageSameAsMace()
{
	testPlayer->addWeapon(*testSword);
	testPlayer->addWeapon(*testMace);
	testPlayer->equipWeapon(*testSword);
	testPlayer->equipWeapon(*testMace);
	CPPUNIT_ASSERT(testPlayer->getDmg() == 60);
}

void TestPlayer::getInventory_swordInInventory_CoutInventory()
{
	testPlayer->addWeapon(*testSword);
	testPlayer->getInventory();
}

void TestPlayer::getInventory_emptyInventory_CoutInventory()
{
	testPlayer->getInventory();
}

void TestPlayer::getInventory_toolInInventory_CoutInventory()
{
	testPlayer->tools.push_back(*testTool);
	testPlayer->hasTools = true;
	testPlayer->getInventory();
}

void TestPlayer::equipTool_equipHammer_hammerEquiped()
{
	testPlayer->tools.push_back(*testTool);
	testPlayer->equipTool(testPlayer->tools[0]);
	CPPUNIT_ASSERT(testPlayer->equipedTool->getName() == testTool->getName());
}

void TestPlayer::destroyItem_emptyFunction_NoThrows()
{
	CPPUNIT_ASSERT_NO_THROW(testPlayer->destroyItem());
}