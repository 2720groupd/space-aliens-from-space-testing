//**************************************************************************************
// TestWeapon File for SpaceAliensFromSpace Tests
//Author: Group D
//Group Assignment 3 CPSC 2720
//**************************************************************************************

#ifndef TESTGAME_H
#define TESTGAME_H

#include "../Game.h"
#include "Player.h"
#include <cppunit/extensions/HelperMacros.h>


class TestGame : public CppUnit::TestFixture
{
    //Macro for creating test suite
    CPPUNIT_TEST_SUITE( TestGame );
    CPPUNIT_TEST(TestGeneralizedUse);
    CPPUNIT_TEST(TestGeneralizedExplore);
    CPPUNIT_TEST(TestGeneralizedGet);
    CPPUNIT_TEST(TestGeneralizedAttack);
    CPPUNIT_TEST(TestGeneralizedWrong);

    CPPUNIT_TEST(TestGameOver);
    CPPUNIT_TEST_SUITE_END();
private:
Game *game;
Creature* creature;
//Player *player;
public:
    void setUp();
    void tearDown();
    void TestGeneralizedUse();
    void TestGeneralizedExplore();
    void TestGeneralizedGet();
    void TestGeneralizedAttack();
    void TestGeneralizedWrong();
    void TestGameOver();

};

#endif