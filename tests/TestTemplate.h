//**************************************************************************************
// Bishop Piece Unit Tests Header File
//Author: Matthew Meier
//Assignment 3 CPSC 2720
//**************************************************************************************

#ifndef TESTBISHOP_H
#define TESTBISHOP_H

#include "../Board.h"
#include "../Piece.h"
#include "../Exceptions.h"
#include <cppunit/extensions/HelperMacros.h>


class TestBishop : public CppUnit::TestFixture
{
    //Macro for creating test suite
    CPPUNIT_TEST_SUITE( TestBishop );
    CPPUNIT_TEST(movePiece_bishopMoveUpLeftDiagonalBoundary_pieceMoved);
    CPPUNIT_TEST(movePiece_bishopMoveDownLeftDiagonalBoundary_pieceMoved);
    CPPUNIT_TEST(movePiece_bishopMoveUpRightDiagonalBoundary_pieceMoved);
    CPPUNIT_TEST(movePiece_bishopMoveDownRightDiagonalBoundary_pieceMoved);
    CPPUNIT_TEST_EXCEPTION(movePiece_bishopMoveVertical_throwExcpetion, invalid_move_error);
    CPPUNIT_TEST_EXCEPTION(movePiece_bishopMoveHorizontal_throwException, invalid_move_error);
    CPPUNIT_TEST_SUITE_END();
private:
    Board *mainBoardBishop;
    Piece *testBishop;
public:
    void setUp();
    void tearDown();

    //Bishop movement tests

    void movePiece_bishopMoveUpLeftDiagonalBoundary_pieceMoved();
    void movePiece_bishopMoveDownLeftDiagonalBoundary_pieceMoved();
    
    void movePiece_bishopMoveUpRightDiagonalBoundary_pieceMoved();
    void movePiece_bishopMoveDownRightDiagonalBoundary_pieceMoved();
    
    //potentially redundant, same path
    void movePiece_bishopMoveVertical_throwExcpetion();
    void movePiece_bishopMoveHorizontal_throwException();
};

#endif