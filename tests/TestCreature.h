//**************************************************************************************
// TestCreature Header File for SpaceAliensFromSpace Tests
//Author: Group D
//Group Assignment 3 CPSC 2720
//**************************************************************************************

#ifndef TESTCREATURE_H
#define TESTCREATURE_H

#include "../Creature.h"
#include <cppunit/extensions/HelperMacros.h>


class TestCreature : public CppUnit::TestFixture
{
    //Macro for creating test suite
    CPPUNIT_TEST_SUITE( TestCreature );
    CPPUNIT_TEST(takeDamage_LessThanFullDamageTaken_HealthFiftyLess);
    CPPUNIT_TEST(takeDamage_MoreThanFullHealthDamageTaken_HpSetZero);
    CPPUNIT_TEST(getHp_returnHp_ReturnInitializedValue);
    CPPUNIT_TEST(setDmg_setNewDamage_damageChanged);
    CPPUNIT_TEST(setHp_setNewHP_HpChanged);
    CPPUNIT_TEST(getDamage_returnDmgValue_returnInitializedValue);
    CPPUNIT_TEST(attack_dmgLessThanHp_setNewHp);
    CPPUNIT_TEST(attack_dmgMoreThanHp_setNewHpZero);
    CPPUNIT_TEST(attack_dmgEqualToHp_setHpZero);
    CPPUNIT_TEST_SUITE_END();
private:
	Creature *testCreature;
	Creature *attackTestCreature;
public:
    void setUp();
    void tearDown();
    void getDamage_returnDmgValue_returnInitializedValue();
    void getHp_returnHp_ReturnInitializedValue();
    void setDmg_setNewDamage_damageChanged();
    void setHp_setNewHP_HpChanged();
    void takeDamage_LessThanFullDamageTaken_HealthFiftyLess();
    void takeDamage_MoreThanFullHealthDamageTaken_HpSetZero();
    void attack_dmgLessThanHp_setNewHp();
    void attack_dmgMoreThanHp_setNewHpZero();
    void attack_dmgEqualToHp_setHpZero();

   
};

#endif