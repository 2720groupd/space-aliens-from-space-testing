//**************************************************************************************
// TestPlayer Header File for SpaceAliensFromSpace Tests
//Author: Group D
//Group Assignment 3 CPSC 2720
//**************************************************************************************

#ifndef TESTPLAYER_H
#define TESTPLAYER_H

#include "../Player.h"
#include "../Weapon.h"
#include "../Tools.h"
#include <cppunit/extensions/HelperMacros.h>


class TestPlayer : public CppUnit::TestFixture
{
    //Macro for creating test suite
    CPPUNIT_TEST_SUITE( TestPlayer );
    CPPUNIT_TEST(addWeapon_addTestSword_swordInInventory);
    CPPUNIT_TEST(checkWeapons_noWeaponPresent_returnFalse);
    CPPUNIT_TEST(checkWeapons_weaponInInventory_returnTrue);
    CPPUNIT_TEST(returnWeapon_swordInInventory_returnSword);
    CPPUNIT_TEST(equipWeapon_equipSword_damageIncreased);
    CPPUNIT_TEST(equipWeapon_equipMaceOverSword_DamageSameAsMace);
    CPPUNIT_TEST(getInventory_swordInInventory_CoutInventory);
    CPPUNIT_TEST(getInventory_emptyInventory_CoutInventory);
    CPPUNIT_TEST(getInventory_toolInInventory_CoutInventory);
    CPPUNIT_TEST(equipTool_equipHammer_hammerEquiped);
    CPPUNIT_TEST(destroyItem_emptyFunction_NoThrows);
    CPPUNIT_TEST_SUITE_END();
private:
    Player *testPlayer;
    Weapon *testSword;
    Weapon *testMace;
    Tools *testTool;
public:
    void setUp();
    void tearDown();
    void addWeapon_addTestSword_swordInInventory();
    void checkWeapons_noWeaponPresent_returnFalse();
    void checkWeapons_weaponInInventory_returnTrue();
    void returnWeapon_swordInInventory_returnSword();
    void equipWeapon_equipSword_damageIncreased();
    void equipWeapon_equipMaceOverSword_DamageSameAsMace();
    void getInventory_swordInInventory_CoutInventory();
    void getInventory_emptyInventory_CoutInventory();
    void getInventory_toolInInventory_CoutInventory();
    void equipTool_equipHammer_hammerEquiped();
    void destroyItem_emptyFunction_NoThrows();

   
};

#endif