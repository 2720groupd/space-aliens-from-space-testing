//**************************************************************************************
// TestCreature Implementation File for SpaceAliensFromSpace Tests
//Author: Group D
//Group Assignment 3 CPSC 2720
//**************************************************************************************

#include "TestCreature.h"

void TestCreature::setUp()
{
	testCreature = new Creature(100,5);
	attackTestCreature = new Creature(100,20);
}

void TestCreature::tearDown()
{
	delete testCreature;
}

void TestCreature::getDamage_returnDmgValue_returnInitializedValue()
{
	CPPUNIT_ASSERT(testCreature->getDmg() == 5);
}

void TestCreature::getHp_returnHp_ReturnInitializedValue()
{
	CPPUNIT_ASSERT(testCreature->getHp() == 100);
}

void TestCreature::setDmg_setNewDamage_damageChanged()
{
	testCreature->setDmg(45);
	CPPUNIT_ASSERT(testCreature->getDmg() == 45);
}

void TestCreature::setHp_setNewHP_HpChanged()
{
	testCreature->setHp(1000);
	CPPUNIT_ASSERT(testCreature->getHp() == 1000);
}

void TestCreature::takeDamage_LessThanFullDamageTaken_HealthFiftyLess()
{
	testCreature->takeDamage(50);
	CPPUNIT_ASSERT(testCreature->getHp() == (50));
}

void TestCreature::takeDamage_MoreThanFullHealthDamageTaken_HpSetZero()
{
	testCreature->takeDamage(2000);
	CPPUNIT_ASSERT(testCreature->getHp() == 0);
}

void TestCreature::attack_dmgLessThanHp_setNewHp()
{
	testCreature->attack(attackTestCreature);
	CPPUNIT_ASSERT(attackTestCreature->getHp() == 95);

}

void TestCreature::attack_dmgMoreThanHp_setNewHpZero()
{
	testCreature->setDmg(1000000000);
	testCreature->attack(attackTestCreature);
	CPPUNIT_ASSERT(attackTestCreature->getHp() == 0);

}

void TestCreature::attack_dmgEqualToHp_setHpZero()
{
	testCreature->setDmg(100);
	testCreature->attack(attackTestCreature);
	CPPUNIT_ASSERT(attackTestCreature->getHp() == 0);

}