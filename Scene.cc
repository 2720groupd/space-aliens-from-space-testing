#include <iostream>
#include <iomanip>
#include <ctime>
#include <algorithm>
#include "Weapon.h"
#include "Scene.h"
#include "Game.h"
#include "Player.h"

using namespace std;

//Scene 1 implementation
bool Scene_1::play(Player* gameUser)
{
   user = gameUser;
   string choice;
  cout << "CHAPTER 1" << endl << endl;
  cout << "WARNING! WARNING! FATAL ERROR! WARNING WARNING!" << endl << "The shuttle is in lockdown, maybe you should 'get up' and see what is happening." << endl;

  options.clear();
  options.push_back("get up"); //[0]
  options.push_back("sleep more"); //[1]

  choice = getUserChoice();

  if (choice == options[0]){ //Get up
    cout << "Looking around you see that the 'console' is flashing red, the escape 'pod' is prepped to launch and a sliced 'cake' from the party you had last night." << endl;
    
    options.clear();
    options.push_back("use cake"); //option[0]
    options.push_back("use pod"); //option[1]
    options.push_back("use console"); //option[2]

    choice = getUserChoice();

    if (choice == options[0]){ //Eat cake
      cout << "You eat the cake, it tastes heavenly..." << endl << "While your eating the cake the ship explodes because you ignored the the bright, red, flashing lights and annoying beeping!" << endl;
      gameOver(1, "because you ignored a fatal error and the ship exploded.");
      return 0;
    }
    if (choice == options[1]){ //Enter escape pod
      cout << "You walk towards the escape pod and slip on some garbage you left on the floor and released the escape pod with the hatch open causeing you to fly out into space and implode... Good bye sweet prince." << endl;
      gameOver(1, "because you slipped into the vacuum of space");
      return 0;
    }
    if (choice == options[2]){ //Interact with the control panel
      cout << "**********************************" << endl;
      cout << "*                                *"<< endl;
      cout << "*   ERROR: Main engine damages   *" << endl;
      cout << "*                                *" << endl;
      cout << "* 'Restart engine': main         *" << endl;
      cout << "* 'Power Down' Shuttle           *" << endl;
      cout << "*                                *" << endl;
      cout << "* 'Self destruct'                *" << endl;
      cout << "*                                *" << endl;
      cout << "**********************************" << endl;
       
       options.clear();
       options.push_back("restart engine");   //options[0]
       options.push_back("power down"); //options[1]
       options.push_back("");          //options[2]
       options.push_back("self destruct");   //options[3]

	   choice = getUserChoice();

       if (choice == options[3]){ //Self-destruct
	   for (int i = 10; i >=0; i--)
	     cout << i << endl;
	   cout<< "Boom! And it was your last trip before retirement..." << endl;
	   gameOver(1, "because you chose to explode the ship while inside it.");
	   return 0;
       }
       if (choice == options[1]){ //Ignore
	 cout << "You shut the entire shuttle down, causing it to fly wildly off into space with no chance of returning home." << endl;
	 gameOver(1, "because you shut down your shuttle.");
	 return 0;
       }
       if (choice == options[2]){ //Remind me later
	 cout << "You blankly stare at the screen, and then it hit you, you died." << endl;
	 gameOver(1, "because you procrastinated.");
	 return 0;
       }
       if (choice == options[0]) //Shut down engine ||Start of Shuttle Puzzle||--------------------------
       {
	  options.clear();
	  options.push_back("use a"); //[0]
	  options.push_back("use b"); //[1]
	  options.push_back("use c"); //[2]
	  options.push_back("use d"); //[3]
	  options.push_back("give up"); //[4]
	  options.push_back("explore note"); //[5]
	  int count = 0; //Time to repair the ship
	  bool aT = false, bT = false, cT = false, success = false;

	  cout << "******************************| A " << endl;
	  cout << "*                             | C " << endl;
	  cout << "*   Enter Shut Down Sequence: |___" << endl;
	  cout << "*                                *" << endl;
	  cout << "*                                *" << endl;
	  cout << "*     |=| A           B |=|      *" << endl;
	  cout << "*                                *" << endl;
	  cout << "*     |=| C           D |=|      *" << endl;
	  cout << "*                                *" << endl;
	  cout << "**********************************" << endl;
	  cout << "off to the corner you can see a 'sticky note' hanging off the console" << endl;

	  do
	  {      
	     choice = getUserChoice();

			//sticky note
			if (choice == options[5]){
			 cout << "The sticky not isn't very legible..." << endl << "How to --...start Main Engin--:" << endl << "When all are ---n---g re-, press A, follo-ed by --, then B. D shoul- be pre---- las-. " << endl << "You can't make sense of the rest" << endl;
		 }

	     if (count == 10)
	     {
				gameOver(1, "Because you didn't read the user manual... The shuttle explodes and you died from stupidity.");
				return 0;
	     }
	 

	     if (choice == options[0] && aT == false)
	     {
		cout << "'A' is no longer flashing red." << endl;
		aT = true;
		count++;
	     }

	     else if (choice == options[2] && aT == true && cT == false)
	     {
		cout << "'C' is no longer flashing red." << endl;
		cT = true;
		count++;
	     }

	     else if (choice == options[1] && cT == true && bT == false)
	     {
		cout << "'B' is no longer flashing red." << endl;
		bT = true;
		count++;
	     }

	     else if (choice == options[3] && bT == true)
	     {
		cout << "'D' is no longer flashing red." << endl << "." << endl << ".." << endl << "..." << endl << "....Main Engines Succesfully reset." << endl;
		success = true;
		count++;
	     }

	     else
	     {
		cout << "That must be the wrong sequence, the buttons are all flashing red." << endl;
		aT = bT = cT = false;
		count++;
	     }
	  }while(success == false);
       }

       //END SHUTTLE PUZZLE
	  
    }
  }
  
  
  if (choice == options[1]){
 
    gameOver(1, "because of your laziness to do anything.");
    return 0;
  }
  return 1;
}


//Scene 2 implementation
bool Scene_2::play(Player* gameUser)
{
   user = gameUser;
   string choice;
   cout << "CHAPTER 2" << endl << endl;
   cout << "You have narrowly escaped death by fixing your ship! \n";
   cout << "Now you can 'explore space' or just look out of the 'window' \n";
   options.push_back("explore space");
   options.push_back("use window");
   choice = getUserChoice();

   if (choice == options[1]) //use window
   {
      cout<<"\nYou look out the window and see a 'planet' in the distance.\n";
      options.clear();
      options.push_back("explore planet");
      choice = getUserChoice();
   }
   if (choice == options[0]) //explore planet
   {
	   cout << "The coordinates haven't been set, the ship doesn't know where to go." << endl;
     
     bool next = false;
     while(!next)
      {
	 options.clear();
	 options.push_back("use system");
	 options.push_back("self destruct");
	 options.push_back("set destination");
	 choice = getUserChoice();

	 if (choice == options[0]) //Check system.
	 {
	    cout<<"System Report\n";
	    cout<<"*************\n";
	    cout<<"Fuel level: No info\n";
	    cout<<"Navigation: No info\n";
	    cout<<"Engines:    Nominal\n";
	    next = false;
	 }
	 else if (choice == options[1]) //Self destruct.
	 {
	    for (int i = 10; i >=0; i--)
	       cout<< i << "\n";
	    cout<< "Boom! You did it, you killed space.\n";
	    return false;
	 }
	 else if (choice == options[2]) //Set a destination.
	 {
	    cout<<"Enter a destination \n";
		cout << "It seems like you can travel to the 'nearest planet', 'home' or set your own coordinates.\n" << endl;
	    options.clear();
	    
	    options.push_back("home planet");
	    options.push_back("nearest planet");
	    options.push_back("custom coordinates");

	    choice = getUserChoice();
	    
	    if(choice == options[0]) //Navigate to home planet.
	    {
	       cout<<"Several months later you arrive at your home planet... \n they are not pleased that you aborted your mission to save them. You have been exiled from your home planet.\n";
	       return false;
	    }
	    else if(choice == options[1]) //Navigate to nearest planet.
	    {
	       cout<<"Destination has been set. You are now on course towards a planet previously unvisited by your people. \n";
	       next = true;
	    }
	    else if(choice == options[2]) //Custom coordinates.
	    {
	       cout<< "You press random buttons hoping to find a cool destination like a planet full of bikini babes. \n Unknown error! System offline! \n";
	       next = false;
	    }
	 }
      }
   }
   cout<<"...Days later you begin to enter the atmosphere of the mysterious planet. \n";
   return true;//Return true if player successfully completes the scene.
}

//***********************
//Scene 3 implementation
bool Scene_3::play(Player* gameUser)
{
    user = gameUser;
    cout << "CHAPTER 3" << endl << endl;
   cout<< "As you descend toward the surface of the planet your ship begins to break apart.\n";

   cout<<"************************ \n";
   cout<<"WARNING! Air Speed too high!\n";
   cout<<"Altitude\n";
   for (int i=15; i>0; i--)
   {
      cout<< i << ",000 Feet\n";
   }
   cout<<"Crash!!!\n";
   cout<<"The landing is rough but you survive. \n";
   cout << "The shuttles main cabin is still intact, perhaps the outside is uninhabitable to humans, maybe you should check the systems on your 'console'." << endl;

   string choice;
   bool next = false;

   while(!next)
   {
      options.clear();
      options.push_back("use window"); //[0]
      options.push_back("use console"); //[1]
      choice = getUserChoice();
      if (choice == options[0]) //use window
      {
	 cout<<"\nYou look out the windows. As the dust begins to clear you catch your first glimpse of the mysterious planet.\n";
      }
      
      else if (choice == options[1]) //interact with console
      {
	 options.clear();
	 options.push_back("self destruct");//[0]
	 options.push_back("use system");//[1]
	 choice = getUserChoice();

	 cout << "**********************************" << endl;
	 cout << "*                                *" << endl;
	 cout << "*         Main Console           *" << endl;
	 cout << "*                                *" << endl;
	 cout << "* 'Check System' : Hobitable     *" << endl;
	 cout << "*                                *" << endl;
	 cout << "*                                *" << endl;
	 cout << "* 'Self destruct'                *" << endl;
	 cout << "*                                *" << endl;
	 cout << "**********************************" << endl;
	 
	 if (choice == options[0]) //self destruct
	 {
	    cout<<"\n*sigh* Not everyone is cut out for this job.\n";
	    for (int i = 10; i >=0; i--)
	       cout<< i << "\n";
	    cout<< "Boom\n";
	    return false;
	 }
	 if (choice == options[1])
	 {
	    cout<<"System Report\n";
	    cout<<"*************\n";
	    cout<<"Fuel level: No info\n";
	    cout<<"Navigation: Location unknown\n";
	    cout<<"Engines:    Engine 2 moderate damage\n";
	    
		cout << "Looks like engine 2 was damaged during landing." << endl << "Maybe you can find something on this planet to fix it with.This could be a problem if you need to make a quick getaway!" << endl;
	    next = true;
	 }
	
      }
   }
   cout<<"\nYou suit up and leave the protection of your ship.\n";
   return true;
}

//********************************
//Scene 4 implementation
bool Scene_4::play(Player* gameUser)
{
   user = gameUser;
   cout << "CHAPTER 4" << endl << endl;
   cout<<"The planet is unlike anything you have seen before.\n";

   string choice;
   
   
      options.clear();
      options.push_back("look around"); //[0] leads to walking around.
      choice = getUserChoice();
      
      if (choice == options[0])
      {
	 cout<<"You look around and observe this strange new planet.\n";
	 cout<<"The sky is an odd shade of light blue, the ground seems to be covered in some sort of green plantmatter.\n";
      }
      
      options.clear();
      options.push_back("explore area"); //[0]
      choice = getUserChoice();
      
      if (choice == options[0]) //explore area.
      {
	 return true;
      }
      return true;
}

//**********************************
// Scene 5 implementation
bool Scene_5::play(Player* gameUser) //Weapon for combat phase is found in this scene. PLayer will die in next scene if they do not collect the weapon.
{
   user = gameUser; //Scene_5's user pointer is set to the address of Game's user pointer.

   string choice;
   bool hasStick = false;
   cout << "CHAPTER 5" << endl << endl;
  
    cout<<"You venture away from your ship to explore the area, in hopes that you will find something to help you repair your ship.\n";
    cout<<"After walking for a while, you stumble upon  what appears to be an abandoned home.\n";

    options.push_back("explore inside"); //[0]
    options.push_back("explore further");//[1]
    choice = getUserChoice();

    if (choice == options[0]) //go inside
    {  
       cout<<"You enter through the front door. The home is dusty and almost completemy empty.\n";
       
       options.clear();
       options.push_back("leave house"); //[0]
       options.push_back("explore around"); //[1]
       choice = getUserChoice();
       
       if (choice == options[1])//loof around.
       {
	  cout<<"The house is mostly empty. There are is a room to the left and a room to the right.\n";
	  options.clear();
	  options.push_back("explore right"); //[0]
	  options.push_back("explore left"); //[1]
	  choice = getUserChoice();
	  	  
	  if (choice == options[0]) //go right
	  {
	     cout<<"You peek inside the room. Nothing but dust bunnies in there.\n";
	     options.clear();
	     options.push_back("leave house"); //[0]
	     options.push_back("explore left");//[1]
	     choice = getUserChoice();
	  }
	  if (choice == options[1])//go left
	  {
	     cout<<"There is nothing in this room except a long pointy stick.\n";
	     options.clear();
	     options.push_back("take stick"); //[0]
	     options.push_back("leave house"); //[1]
	     choice = getUserChoice();
	     
	     if (choice == options[0]) //****Player needs to collect the stick to win fight.
	     {
		hasStick = true;
		Weapon stick("stick", 20);
		user->addWeapon(stick);
		cout<<"Careful, that could poke somebone's eye out! Content with your new found possesion you continue on your way.\n";
	     }
	     else if(choice == options[0])
	     {
		cout<<"You leave the house and continue on your way.\n";
	     }
	  }
       }
    }
    if(hasStick == true)
    {
       cout<<"Content with your new found possesion you continue on your way.\n";
    }
    else
       cout<<"You continue exploring the mysterious planet.\n";

    //   user->getInventory();
   return true;
}

//*****************************
// Scene 6 implementation
bool Scene_6::play (Player* gameUser) //player will die if they do not have stick as a weapon.
{
   user = gameUser;//Scene_5's user pointer is set to the address of Game's user pointer.
   
   string choice;
   cout << "CHAPTER 6" << endl << endl;
   cout<<"A short while later you hear rustling in some bushes. A creature appears.\n";
   // user->getInventory(); 

   options.push_back("speak with"); //[0]
   options.push_back("attack creature"); //[1]
   choice = getUserChoice();

   enum response {snarl, wimper, laugh};

   if (choice == options[0]) //speak with creature
   {
      int response;
      srand(time(NULL));
      response = rand() % 3;
      
      if(response == snarl)
      {
	 cout<<"The creature responds with a snarl!\n";
      }
      else if(response == laugh)
      {
	 cout<<"The creature laughs at you!\n";
      }
      else if(response == wimper)
      {
	 cout<<"The creature wimpers.\n";
      }

      options.clear();
      options.push_back("attack creature"); //[0]
      options.push_back("run away"); //[1]
      choice = getUserChoice();

      if(choice == options[0]) //Attack creature
      {
	 options.clear();
	 options.push_back("use fists");
	 if(user->checkWeapons() != false)
	 {
	    cout<<"Would you like to use your sharp stick or bare fist it?\n";
	    options.push_back("use stick");
	 }
	 choice = getUserChoice();

	 if(choice == options[0]) //use fists.
	 {
	     cout<<"Fisty cuffs it is!\n";
	 }
	 else if (choice == options[1]) //use stick.
	 {
	    user->equipWeapon(user->returnWeapon(0));
	 }
     
	 user->getInventory();
	 Creature* alien = new Creature(60, 20);
	 cout<<"\nHere is your damage: "<< user->getDmg() << endl;
	 battle (alien);
		 
	 if(user->getHp() <= 0)
	 {
	    gameOver(6, "The alien killed you. Maybe if you had collected a weapon you would have one.");
	     gameOver(6, "The alien caught up to you and killed you!");
	     return false;
	 }
	 
      }
      else if(choice == options[1]) //Run away.
      {
	 cout<<"You make a break for it!\n";
	 cout<<"You can hear the alien catching up to you!\n";
	 cout<<"The alien catches up to you and kills you!\n";
	
      }
      
   }
   else if(choice == options[1]) //attack creature.
   {
      options.clear();
	 options.push_back("use fists");
	 if(user->checkWeapons() != false)
	 {
	    cout<<"Would you like to use your sharp stick or bare fist it?\n";
	    options.push_back("use stick");
	 }
	 choice = getUserChoice();

	 if(choice == options[0]) //use fists.
	 {
	     cout<<"\nFisty cuffs it is!\n";
	 }
	 else if (choice == options[1]) //use stick.
	 {
	    user->equipWeapon(user->returnWeapon(0));
	 }
     
	 user->getInventory();
	 Creature* alien = new Creature(60, 20);
	 cout<<"\nHere is your damage: "<< user->getDmg() << endl;
	 battle (alien);
		 
	 if(user->getHp() <= 0)
	 {
	    gameOver(6, "The alien killed you. Maybe if you had collected a weapon you would have one.");
	    gameOver(6, "The alien caught up to you and killed you!");
	    return false;
	 }
	 
   }
   
   return true;
}

//***********************************
// Scene 7 implementation
bool Scene_7::play(Player* gameUser)
{
   user = gameUser;
   string choice;
   cout << "CHAPTER 7" << endl << endl;
   cout<<"The injured alien kneels before you, humbled by your bravery and skillful combat techniques.\n";

   options.push_back("walk away"); //[0]
   options.push_back("talk to"); //[1]
   choice = getUserChoice();

   if(choice == options[1]) //Speak with the alien. Leads to scene 8a or 8b.
   {
      cout<<"Thank you! I didn't mean any harm, I just wanted to talk to you.\n";

      options.clear();
      options.push_back("walk away"); //[0]
      options.push_back("ask question"); //[1]
      choice = getUserChoice();
      
      if(choice == options[1]) //question the alien.
      {
	 bool name = false;
	 bool next = false; 
	 while(!next)
	 {
	    cout<<"What do you want to ask the alien?\n";

	    if(name == false)
	    {
	       options.clear();
	       options.push_back("ask intentions"); //[0]
	       options.push_back("ask name"); //[1]
	       
	       choice = getUserChoice();
	    }
	    else if(name == true)
	    {
	       options.clear();
	       options.push_back("ask intentions"); //[0]
	       choice = getUserChoice();
	    }
	    if(choice == options[1]) //ask name
	    {
	       cout<<"It would take a thousand lunar cycles to say my name in your tongue. Just call me Jabu./n";
	       name = true;
	    }
	    else if(choice == options[0]) //ask intentions
	    {
	       if (name != true)
		  cout<<"It would take a thousand lunar cycles to say my name in your tongue. Just call me Jabu.\n";
	       cout<<"I meant you no harm. I only wanted to ask for your help. My people's planet is in trouble and you can save us! An evil creature wishes to enslave us! \nPlease, will you help us?\n";
	       options.clear();
	       options.push_back("help alien");//[0]
	       options.push_back("walk away");//[1]
		   options.push_back("evil creature");//[2]
	       choice = getUserChoice();

	       if (choice == options[0]) //Help alien.
	       {
		  cout<<"Thank you for your kindness! I know you can help us, follow me!\n";
		  return true;
		  //leads to scene 8b.
	       }
	       else if(choice == options[1]) //Walk away
	       {
		  next = true;
	       }
		   else if (choice == options[2]) //evil creature
		   {
			   cout << "Yes a ghost, in the caves on the other side of that valley wishes to enslave us. If no one can complete the trial we are doomed!" << endl;
		   }
	       
		   options.clear();
		   options.push_back("help alien");//[0]
		   options.push_back("walk away");//[1]

			choice = getUserChoice();

		   if (choice == options[0]) //Help alien.
		   {
			   cout << "Thank you for your kindness! I know you can help us, follow me!\n";
			   return true;
			   //leads to scene 8b.
		   }
		   else if (choice == options[1]) //Walk away
		   {
			   next = true;
		   }
	    }
	 }
      }
   }
   //If player choose to walk away at any point.
   cout<<"Fine, have it your way!\n";
   return false;
   //leads to scene 8a.
}

//***********************************
// Scene 8 implementation (player declines to help alien.)
bool Scene_8::play(Player* gameUser)
{
   user = gameUser;
   cout << "CHAPTER 8b" << endl << endl;
   string choice;
   cout<<"You walk away from the alien you left beaten and bruised, continuing to explore the planet.\n";
   cout<<"Wham!! Something hits you across the back of your head and you are knocked unconscious!\n";
   cout<<"A short while later you awake.\n";

   options.push_back("explore around");//[0]
   choice = getUserChoice();

   if(choice == options[0])//look around
   {
      cout<<"You pick yourself up off of the cold damp ground. It appears you are locked in a cell...\n";
   }
   options.clear();
   options.push_back("call out");//[0]
   choice = getUserChoice();
   if(choice == options[0]) //call out
   {
      //Feel free to modify.
      cout<<"You call out, ''Hey! Is anyone there!?''\n";
      cout<<".\n";
      cout<<".\n";
      cout<<".\n";
      cout<<"There is no response. \n";
   }

   //*****puzzle in this scene to escape prison. May result in death.

   int count = 0, rocks = 3, rock = 0;
   bool tricked = false, inCell = false, away = false, success = false;

   cout << "You decide to rest a little longer..." << endl;
   cout << "'Hey! Wake Up! I can't let you die before your execution!', a lumbering alien is staring at you. 'Good, just stay quiet.'" << endl << "The guard hands you a plate with what look like soggy bread on it." << endl;

   do {

	   options.clear();
	   options.push_back("call out");//[0]
	   options.push_back("explore around");//[1]
	   options.push_back("use bread");//[2]
	   options.push_back("fake sick");//[3]
	   options.push_back("attack guard");//[4]
	   options.push_back("take rock");//[5]
	   options.push_back("use rock");//[6]
	   options.push_back("use bed");//[7]

	   choice = getUserChoice();

	   if (count == 10)
	   {
		   cout << "The guards opens the cell and grabs you by your collar. You are dragged out into the desert where the bury you in the sand. Good bye sweet prince..." << endl;
		   //add game over
		   
		   gameOver(1, "You were left to die in a desert.");
		   return 0;
	   }

	   else if (choice == options[1])
	   {
		   cout << "The cell is small, it smells gross and it's damp. You see a bed, looks like it hasn't been washed in years. There are a bunch of rocks on the ground. A guard hands you a plate with soggy bread on it." << endl;
		   count++;
	   }

	   else if (choice == options[0])
	   {
		   cout << "The guard looks at you like you're an idiot." << endl << "Guard: 'Are you and idiot?'" << endl;
		   count++;
		   tricked = inCell = away = false;
	   }

	   else if (choice == options[2])
	   {
		   cout << "You take a bite of the bread, it tastes as good as you thought. Your stomach starts to hurt." << endl;
		   count++;
		   tricked = true;
	   }

	   else if (tricked == true && choice == options[3])
	   {
		   cout << "The guard seems worried that the bread made you sick. You need to be properly executed after all!" << "The guard opens the cell and walks in to check on you." << endl;
		   count++;
		   inCell = true;
	   }

	   else if (inCell == true && choice == options[4])
	   {
		   cout << "You quickly attack the guard, a swift kick to his jaw and you knocked him out." << endl;
		   success = true;
	   }

	   else if (choice == options[5])
	   {
		   cout << "You pick up a rock on the ground. It looks like there are only " << rocks << " left" << endl;
		   rock++;
		   count++;
	   }

	   else if (rock > 0 && choice == options[6])
	   {
		   cout << "You throw the rock accross the dungeon. The guard wanders off to see what that noise is." << endl;
		   away = true;
		   rock--;
		   count++;
	   }

	   else if (away == true && choice == options[7])
	   {
		   cout << "You hide under the bed, immediately regretting the decision because of the foul smell." << endl << "The guard returns but can't find you, in a panic he unlocks the cell and come in." << endl;
		   count++;
		   inCell = true;
	   }

   } while (success == false);
   //Prison Puzzle End============================================================================
   
   return true;
}

//***********************************
// Scene 9 implementation (continuation of scene 8. Puzzle completed successfully.)
bool Scene_9::play(Player* gameUser)
{
   user = gameUser;
   cout << "CHAPTER 9b" << endl << endl;
   string choice;
   cout<<"You have successfully escaped the prison! Happy to be alive and free you begin to make your way back towards your ship.\n";
   cout<<"Just as you think you are out of danger a spooky ghost appears.\n";
   cout<<"The ghost speaks to you. ''What is your name, brave explorer?''\n";
   
   options.push_back("tell name"); //[0]
   options.push_back("say nothing"); //[1]
   choice = getUserChoice();

   if(choice == options[0]) //tell name.
   {
      cout<<"The ghost responds, ''You have proven yourself worthy. Your bravery in combat and superior puzzle solving skills have shown me that you are a great explorer.''\n";
	 cout<<"''If you can solve one final puzzle I shall present you with a gift of great importance.''\n";
   }

   else if(choice == options[1]) //say nothing.
   {
      cout<<"The ghost speaks to you again. ''Do not be afraid, I have come to congratulate you on your bravery in combat and superior puzzle solving skills. If you can solve one final puzzle I shall present you with a gift of great importance.''\n";
   }

   //Ghost Riddles==========================================================================================================
   int badMood = 0, question = 1;
   bool success = false;

   cout << "Who makes it, has no need of it." << endl
	   << "Who buys it, has no use for it." << endl
	   << "Who uses it can neither see nor feel it." << endl
	   << "What is it?" << endl;

   while (success != true && badMood != 2)
   {
	   options.clear();
	   options.push_back("a coffin");//[0]
	   options.push_back("a towel");//[1]
	   options.push_back("a palm");//[2]
	   choice = getUserChoice();

	   if (choice == options[0] && question == 1)
	   {
		   cout << "That is correct. Onto the next one." << endl;
		   cout << "What gets wetter and wetter the more it dries?" << endl;
		   question++;
	   }
	   else if (choice == options[1] && question == 2)
	   {
		   cout << "You are quite intelligent. The final question." << endl;
		   cout << "What kind of tree can you carry in your hand?" << endl;
		   question++;
	   }
	   else if (choice == options[2] && question == 3)
	   {
		   cout << "That is all the riddles, perhaps you are the one to make a difference!" << endl;
		   success = true;
	   }

	   else if ((question == 1 && choice != options[0]) || (question == 2 && choice != options[1]) || (question == 3 && choice != options[2]))
	   {
		   if (badMood == 0)
		   {
			   cout << "No... No! That is not correct, you are supposed to be the one to save everyone!" << endl;
		   }
		   if (badMood == 1)
		   {
			   cout << "This is impossible... I was sure you were the one..." << endl;
		   }

		   badMood++;
	   }

   }
   if (success == true)
   {
	   cout << "Here, take this orb, it will help you destroy the evil creatures." << endl << "They are all over this planet, watch out, they may try and trick you." << endl;
	   return true;
   }
   else
   {
	   cout << "But I was wrong to think you could make a difference, instead I will end your life now!" << endl;
	   gameOver(1, "The ghost determined you couldn't save everyone from the evil creatures...");
	   return 0;
   }
}//End Ghost Riddle===========================================================================================


//***********************************
// Scene 10 implementation (Player agrees to help the alien.)
bool Scene_10::play(Player* gameUser)
{
   user = gameUser;
   string choice;
   cout << "CHAPTER 8a" << endl << endl;
   cout<<"You follow the alien for a while and arrive at a cave.\n";
   cout<<"You go inside to find many aliens gathered there.\n";
   cout<<"The alien you attacked speaks to you. ''Behind that door lies an object that will save our planet. If you can get it for us we will be indebted to you forever.''\n";

      //word game/ puzzle should go here.============================================================================

   int count = 0;
   bool success = false, leversOn = false, tapestryOn = false, star = true, moon = false, sun = true;

   options.clear();
   options.push_back("explore tapestry");//[0]
   options.push_back("explore levers");//[1]
   options.push_back("star lever");//[2]
   options.push_back("moon lever");//[3]
   options.push_back("sun lever");//[4]
   choice = getUserChoice();
   
   cout << "Upon entering the cave of trials the cave collapses behind you, looks like there is no way to go but forward..." << endl << "Eventually you reach a dead end, suddenly the stones around you begin glowing and you can see a 'tapestry' and three 'levers' on the wall" << endl;
   do
   {
	   if (choice == options[0])
	   {
		   cout << "Taking a closer look at the tapestry you realize it is incomplete." << endl << "The light emited from the stones doesn't only lights up 2 of the 3 tapestries, the left most and right most of the tapestry is lit." << endl;
		   tapestryOn = true;
		   count++;
	   }
	   else if (choice == options[1])
	   {
		   cout << "The levers have symbols above them, one is a 'star' the next is a 'moon' and the third is the 'sun'." << endl;
		   leversOn = true;
		   count++;
	   }
	   else if ((choice == options[2] && tapestryOn == false) || (choice == "moon level" && tapestryOn == false) || (choice == "sun lever" && tapestryOn == false))
	   {
		   cout << "You aren't sure what happened maybe you should 'look' at the 'tapestry'." << endl;
		   count++;
	   }
	   //Star Lever Boolean Start========================================================================
	   else if (choice == options[2] && leversOn == true && star == false && moon == false && sun == false)
	   {
		   cout << "The left tapestry's light turns on, the middle tapestry's light turns on, the right tapestry is still dark." << endl;
		   star = moon = true;
		   count++;
	   }
	   else if (choice == options[2] && leversOn == true && star == false && moon == false && sun == true)
	   {
		   cout << "The three tapestries are all light..." << endl << "Slowly the tapesteries open up leading to another room..." << endl;
		   success = true;
	   }
	   else if (choice == options[2] && leversOn == true && star == false && moon == true && sun == false)
	   {
		   cout << "The left tapestry's light turns on, the middle tapestry's light turns off, the right tapestry is still dark." << endl;
		   star = moon = false;
		   count++;
	   }
	   else if (choice == options[2] && leversOn == true && star == false && moon == true && sun == true)
	   {
		   cout << "The left tapestry's light turns on, the middle tapestry's light turns off, the right tapestry is still lit." << endl;
		   star = true;
		   moon = false;
		   count++;
	   }
	   else if (choice == options[2] && leversOn == true && star == true && moon == false && sun == false)
	   {
		   cout << "The left tapestry's like turns off, the middle tapestry's light turns on, the right tapestry is still dark." << endl;
		   star = false;
		   moon = true;
		   count++;
	   }

	   else if (choice == options[2] && leversOn == true && star == true && moon == false && sun == true)
	   {
		   cout << "The left tapestry's light turns off, the middle tapestry's light turns on, and the right tapestry is still lit." << endl;
		   star = false;
		   moon = true;
		   count++;
	   }

	   else if (choice == options[2] && leversOn == true && star == true && moon == true && sun == false)
	   {
		   cout << "The left tapestry's light turns off, the midd tapestry's light turns off, and the right tapesrty is still dark." << endl;
		   star = moon = false;
		   count++;
	   }
	   //Star Lever Boolean End========================================================================

	   //Moon Lever Boolean Start========================================================================
	   else if (choice == options[3] && leversOn == true && star == false && moon == false && sun == false)
	   {
		   cout << "The three tapestries are all light..." << endl << "Slowly the tapesteries open up leading to another room..." << endl;
		   success = true;
	   }

	   else if (choice == options[3] && leversOn == true && star == false && moon == false && sun == true)
	   {
		   cout << "The left tapestry's light turns on, the middle tapestry's light turns on, and the right tapestry's like turns off." << endl;
		   star = moon = true;
		   sun = false;
		   count++;
	   }

	   else if (choice == options[3] && leversOn == true && star == false && moon == true && sun == false)
	   {
		   cout << "The left tapestry's light turns on, the middle tapestry's light turns off, and the right tapestry's light turns on." << endl;
		   star = sun = true;
		   moon = false;
		   count++;
	   }

	   else if (choice == options[3] && leversOn == true && star == false && moon == true && sun == true)
	   {
		   cout << "The left tapestry's light turns on, the middle tapestry's light turns off, and the right tapestry's light turns off." << endl;
		   star = true;
		   moon = sun = false;
		   count++;
	   }

	   else if (choice == options[3] && leversOn == true && star == true && moon == false && sun == false)
	   {
		   cout << "The left tapestry's light turns off, the middle tapestry's light turns on, and the right tapestry's light turns on." << endl;
		   moon = sun = true;
		   star = false;
		   count++;
	   }

	   else if (choice == options[3] && leversOn == true && star == true && moon == false && sun == true)
	   {
		   cout << "The left tapestry's light turns off, the middle tapestry's light turns on, and the right tapestry's like turns off." << endl;
		   star = sun = false;
		   moon = true;
		   count++;
	   }

	   else if (choice == options[3] && leversOn == true && star == true && moon == true && sun == false)
	   {
		   cout << "The left tapestry's light turns off, the middle tapestry's light turns off, and the right tapestry's light turns on." << endl;
		   star = moon = false;
		   sun = true;
		   count++;
	   }
	   //Moon Lever Boolean End========================================================================

	   //Sun Lever Boolean Start========================================================================
	   else if (choice == options[4] && leversOn == true && star == false && moon == false && sun == false)
	   {
		   cout << "The left tapestry is still dark, the middle tapestry's light turns on, and the right tapestry's light turns on." << endl;
		   star = false;
		   moon = sun = true;
		   count++;
	   }

	   else if (choice == options[4] && leversOn == true && star == false && moon == false && sun == true)
	   {
		   cout << "The left tapestry is still dark, the middle tapestry's light turns on, and the right tapestry's light turns off." << endl;
		   star = sun = false;
		   moon = true;
		   count++;
	   }

	   else if (choice == options[4] && leversOn == true && star == false && moon == true && sun == false)
	   {
		   cout << "The left tapestry is still dark, the middle tapestry's light turns off, and the right rapestry's light turns on." << endl;
		   star = moon = false;
		   sun = true;
		   count++;
	   }

	   else if (choice == options[4] && leversOn == true && star == false && moon == true && sun == true)
	   {
		   cout << "The left tapestry is still lit, the middle tapestry's light turns off, the right tapestry's light turns off." << endl;
		   moon = sun = false;
		   count++;
	   }

	   else if (choice == options[4] && leversOn == true && star == true && moon == false && sun == false)
	   {
		   cout << "The three tapestries are all light..." << endl << "Slowly the tapesteries open up leading to another room..." << endl;
		   success = true;
	   }

	   else if (choice == options[4] && leversOn == true && star == true && moon == false && sun == true)
	   {
		   cout << "The left tapestry is still lit, the middle tapestry's light turns on, and the right tapestry's light turns off." << endl;
		   sun = false;
		   moon = true;
		   count++;
	   }

	   else if (choice == options[4] && leversOn == true && star == true && moon == true && sun == false)
	   {
		   cout << "The left tapestry is still lit, the middle tapestry's light turns off, and the right tapestry's light turns on." << endl;
		   moon = false;
		   sun = true;
		   count++;
	   }
	   //Sun Boolean Lever End========================================================================
	   else
	   {
		   cout << "You can't seem to figure out the puzzel..." << endl;
		   count++;
	   }

   } while (success != true && count < 20); //End lever puzzle

   if (success == true)
	   return true;

   else
	   gameOver(1, "The cave crumbled around you, looks like you failed both yourself and the aliens.");
	   return 0;

   //Cave of trial puzzle end=======================================================================================
}

//***********************************
// Scene 11 implementation (continuation of scene 10.)
bool Scene_11::play(Player* gameUser)
{
   user = gameUser;
   string choice;
   cout << "CHAPTER 9a" << endl << endl;
   
   cout<<"Having successfully completed the puzzle, the door opens.\n";
   options.push_back("explore inside");
   choice = getUserChoice();//[0]

   if(choice == options[0]) //go inside.
   {
      cout<<"You enter the room.\n";
      cout<<".\n";
      cout<<".\n";
      cout<<".\n";
      cout<<"SLAM!! The door closes behind you. You are now in complete dark and silence.\n";
   }
   options.clear();
   options.push_back("start crying");//[0]
   options.push_back("say hello");//[1]
   choice = getUserChoice();

   if(choice == options[0])//start crying.
   {
      cout<<"You begin to cry, wondering why you ever agreed to come to this dumb planet.\n";
   }
   else if(choice == options[1])//say hello.
   {
      cout<<"You call out, ''Hello! Is anyone there?!''\n";
   }
   cout<<"Suddenly there is a blinding flash of light! A ghostly spirit appears.\n";


   cout<<"The ghost speaks to you. ''What is your name, brave explorer?''\n";

   options.clear();
   options.push_back("tell name"); //[0]
   options.push_back("say nothing"); //[1]
   choice = getUserChoice();
   
   if(choice == options[0]) //tell name.
   {
      cout<<"The ghost responds, ''You have proven you are a great explorer. Your bravery in combat and superior puzzle solving skills have shown me that you are worthy.''\n";
      cout<<"''If you can solve one final puzzle I shall present you with a gift of great importance and allow you to pass.''\n";
   }
   
   else if(choice == options[1]) //say nothing.
   {
      cout<<"The ghost speaks to you again. ''Do not be afraid, I have come to congratulate you on your bravery in combat and superior puzzle solving skills. If you can solve one final puzzle I shall present you with a gift of great importance and allow you to pass.''\n";
   }

   //Ghost Riddles==========================================================================================================
   int badMood = 0, question = 1;
   bool success = false;

   cout << "Who makes it, has no need of it." << endl
	   << "Who buys it, has no use for it." << endl
	   << "Who uses it can neither see nor feel it." << endl
	   << "What is it?" << endl;

   while (success != true && badMood != 2)
   {
	   options.clear();
	   options.push_back("a coffin");//[0]
	   options.push_back("a towel");//[1]
	   options.push_back("a palm");//[2]
	   choice = getUserChoice();

	   if (choice == options[0] && question == 1)
	   {
		   cout << "That is correct. Onto the next one." << endl;
		   cout << "What gets wetter and wetter the more it dries?" << endl;
		   question++;
	   }
	   else if (choice == options[1] && question == 2)
	   {
		   cout << "You are quite intelligent. The final question." << endl;
		   cout << "What kind of tree can you carry in your hand?" << endl;
		   question++;
	   }
	   else if (choice == options[2] && question == 3)
	   {
		   cout << "That is all the riddles, perhaps you are the one to make a difference!" << endl;
		   success = true;
	   }

	   else if ((question == 1 && choice != options[0]) || (question == 2 && choice != options[1]) || (question == 3 && choice != options[2]))
	   {
		   if (badMood == 0)
		   {
			   cout << "No... No! That is not correct, you are supposed to be the one to save everyone!" << endl;
		   }
		   if (badMood == 1)
		   {
			   cout << "This is impossible... I was sure you were the one..." << endl;
		   }

		   badMood++;
	   }

   }
   if (success == true)
   {
	   cout << "Here, take this orb, it will help you destroy the evil creatures." << endl << "They are all over this planet, watch out, they may try and trick you." << endl;
	   return true;
   }
   else
   {
	   cout << "But I was wrong to think you could make a difference, instead I will end your life now!" << endl;
	   gameOver(1, "The ghost determined you couldn't save everyone from the evil creatures...");
	   return 0;
   }
}//End Ghost Riddle===========================================================================================


//***********************************
// Scene 12 implementation (final scene.)
bool Scene_12::play(Player* gameUser)
{
   user = gameUser;
   string choice;
   cout << "CHAPTER 10" << endl << endl;

   cout << "Suddenly you're back in the middle of the desert..." << endl << "There's an orb in your hands, it looks like a giant cats eye marble" << endl;
   cout << "What should you do?" << endl;

   options.clear();
   options.push_back("explore home");//[0]
   options.push_back("use orb");//[1]
   options.push_back("find aliens");//[2]
   options.push_back("destroy orb");//[3]
   choice = getUserChoice();

   if (choice == options[0])
   {
	   cout << "As you head back to your shuttle you hear someone shouting in the distance." << endl << "'Hey you! You have the orb! Give that to us!'" << endl;
	   cout << "It's the aliens from before..." << endl;

	   options.clear();
	   options.push_back("run away");//[0]
	   options.push_back("use orb");//[1]
	   options.push_back("talk to");//[2]
	   options.push_back("destroy orb");//[3]
	   choice = getUserChoice();

	   if (choice == options[0])
	   {
		   cout << "Stop! We need that orb!" << endl << "The aliens continue chasing you." << endl << "You just reached your ship, you can try and escape." << endl;

		   options.clear();
		   options.push_back("run away");//[0]
		   options.push_back("talk to");//[1]
		   choice = getUserChoice();

		   if (choice == options[0])
		   {
			   cout << "You get into your ship but forgot it wasn't repaired... The aliens now have you surrounded! 'Come out and we won't kill you!'" << endl;

			   choice = getUserChoice();

			   if (choice == options[0])
			   {
				   cout << "You try to launch the shuttle again... It doesn't start." << endl << "The aliens break into your shuttle! 'Kill him!'" << endl;
				   gameOver(1, "The aliens kill you and take the orb, it turns out the were the evil creatures all along...");
				   return 0;
			   }

			   if (choice == options[1])
			   {
				   cout << "You exit the shuttle and raise you hands. The aliens slowly approach you." << endl << "The second they get in striking range the alien thrust his spear into your chest" << endl;
				   cout << "'Silly humans... They believe anything you tell them!'" << endl;
				   gameOver(1, "The aliens leave you corpse and begin preperations to assault the rest of your kin.");
				   return 0;
			   }

	   }
	   if (choice == options[1])
	   {
		   cout << "You squeeze the orb tight... Eventually it start to get warm and begins vibrating!" << endl;
		   gameOver(1, "The orb explodes destroying you and the entire planet");
		   return 0;
	   }
		
	   }
   }

   if (choice == options[3])
   {
	   cout << "You throw the orb against the closest rock... The orb shatters and giant flames errupt from it..." << endl;
	   gameOver(1, "The orb explodes destroying you and the entire planet");
	   return 0;
   }

   if (choice == options[2])
   {
	   cout << "You turn around and raise you hands. The aliens slow down as they approach you." << endl << "The second they get in striking range the alien thrust his spear into your chest" << endl;
	   cout << "'Silly humans... They believe anything you tell them!'" << endl;
	   gameOver(1, "The aliens leave you corpse and begin preperations to assault the rest of your kin.");
	   return 0;
   }

   if (choice == options[1])
   {
	   cout << "You search for the aliens..." << endl << "'There he is! Hey you! With the orb!' It looks like the aliens found you." << endl;

	   options.clear();
	   options.push_back("run away");//[0]
	   options.push_back("talk to");//[1]
	   choice = getUserChoice();


	   if (choice == options[1])
	   {
		   cout << "You slowly approach the aliens..." << endl << "The second they get in striking range the alien thrust his spear into your chest" << endl;
		   cout << "'Silly humans... They believe anything you tell them!'" << endl;
		   gameOver(1, "The aliens leave you corpse and begin preperations to assault the rest of your kin.");
		   return 0;
	   }

	   if (choice == options[0])
	   {
		   cout << "'No he tricked us! After him!'" << endl;
		   cout << "You keep running eventually finding yourself caught on a cliff edge... That 'orb' might be exactly what you need." << endl;

		   options.clear();
		   options.push_back("use orb");//[0]
		   options.push_back("destroy orb");//[1]
		   choice = getUserChoice();

		   if (choice == options[1])
		   {
			   cout << "You throw the orb against the closest rock... The orb shatters and giant flames errupt from it..." << endl;
			   gameOver(1, "The orb explodes destroying you and the entire planet");
			   return 0;
		   }

		   if (choice == options[0])
		   {
			   cout << "You squeeze the orb tight... Eventually it start to get warm and begins vibrating!" << endl;
			   gameOver(1, "The orb explodes destroying you and the entire planet");
			   return 0;
		   }
	   }
   }
    return 0;
}


MainMenu::MainMenu()
{
  string choice;

  //Creates big empty game simulating a clear screen
  for (int j=0; j<40; j++)
    cout << endl;

  cout << "****************************************" << endl;
  cout << "*       Space Aliens From Space!       *" << endl;
  cout << "*                                      *" << endl;
  cout << "*                                      *" << endl;
  cout << "*   New Game                           *" << endl;
  cout << "*   Continue                           *" << endl;
  cout << "*   Credits                            *" << endl;
  cout << "*   Quit                               *" << endl;
  cout << "*                                      *" << endl;
  cout << "****************************************" << endl;
  cout << endl;

  options.clear();
  options.push_back("new game"); //[0]
  options.push_back("continue"); //[1]
  options.push_back("credits"); //[2]
  options.push_back("quit"); //[3]
  choice = getUserChoice();

  if (choice == options[2]) //credits
  {
	  cout << "This game was made for CPSC 2720 - Practical Software Design" << endl;
	  cout << "at the University of Lethbridge.";
	  cout << "Game creators:" << endl;
	  cout << "EJJ Enterprises," << endl;
	  cout << "Eric Panich" << endl;
	  cout << "Jason Martens" << endl;
	  cout << "John Emerson" << endl;

	  choice = getUserChoice();
  }

  //if new game start game
  if (choice == options[0]){
    playGame();
  }

  if (choice == options[1]){
    int number; //User number for scene to start
	char scenario; //determines which path
	//range check
	bool rangeC = false;
	while (rangeC != true)
	{
		cout << "Enter Chapter: ";
		cin >> number;

		//Clears rest of input
		cin.clear();
		while (cin.peek() != '\n')
			cin.ignore(1);
		cin.ignore();
		cin.clear();

		rangeC = true;

		if (number <= 0)
		rangeC = false;
		
		if (number > 10)
		rangeC = false;


		if (number == 10)
			number = 12;

		bool check = false;

		if (number == 8 || number == 9)
		{
			cout << "Which scene, A or B?" << endl;

			while (check != true)
			{ //start while check

				cin >> scenario;
				cin.clear();
				while (cin.peek() != '\n')
					cin.ignore(1);
				cin.ignore();
				cin.clear();

				if ((number == 8 && scenario == 'A') || (scenario == 'a' && number == 8))
				{
					number = 10;
					check = true;
				}
				else if ((number == 9 && scenario == 'A') || (scenario == 'a' && number == 9))
				{
					number = 11;
					check = true;
				}
				else if ((number == 8 && scenario == 'B') || (scenario == 'b' && number == 8))
				{
					number = 8;
					check = true;
				}
				else if ((number == 9 && scenario == 'B') || (scenario == 'b' && number == 9))
				{
					number = 9;
					check = true;
				}
				else
					cout << "Please input 'A' or 'B'" << endl;
			}
		}//end while check
	}
    //Plays the scene of number "number"
    playScene(number); //will play the scene 
  }

  if (choice == options[3])
  {
	  cout << "TODO!" << endl;
  }
}
