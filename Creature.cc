#include "Creature.h"

Creature::Creature()
{

}

//Initilizes a "creature" and assigns the necassary values
Creature::Creature(int h, int d)
{
    hp = h;     //Assign health value
    dmg = d;    //Assign damage value
}

//Creature attacks target
void Creature::attack(Creature *target){ 
  target->takeDamage(dmg);
  if (target->getHp()<0)
    target->setHp(0);
}

//
void Creature::takeDamage(int d){ 
  hp=hp-d; 
  if (hp<0)
    hp=0;
}

//Setters
void Creature::setDmg(int d){ dmg=d; }
void Creature::setHp(int h){ hp=h; }

//Getters
int Creature::getDmg(){ return dmg; }
int Creature::getHp() { return hp;  }
