#ifndef __WEAPON_H_
#define __WEAPON_H_

#include <string>
using namespace std;

///Represents the weapons used in the game.
class Weapon
{
private:
    int wDmg;  //The damage that the user will gain when this weapon will be equiped
    string name; //Contains name of the weapon

  public:
    ///Constructor. Takes a string and an integer as argument to give the weapon a name and set its Dmg.
    Weapon(string n, int dmg);

    ///Return the damge of a given weapon
    int strength();     

    ///Returns the name of the weapon
    string getName(); 
};

#endif // WEAPON_H
