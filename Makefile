CC=g++11
CFLAGS=-Wall -g -std=c++11 -fprofile-arcs -ftest-coverage

TEST_SRC=tests

OBJ_DIR=obj
OBJS= Creature.o Player.o Tools.o Weapon.o Game.o Scene.o


TEST_OBJS= $(TEST_SRC)/RunTests.o $(TEST_SRC)/TestPlayer.o $(TEST_SRC)/TestTools.o $(TEST_SRC)/TestWeapon.o $(TEST_SRC)/TestCreature.o $(TEST_SRC)/TestGame.o

INCLUDE= -I .

GCOV = gcov11
COVERAGE_RESULTS = result.coverage

PROGRAM=SpaceAliensFromSpace
PROGRAM_TEST=testSpaceAliens

.PHONY: all
all: $(PROGRAM) $(PROGRAM_TEST)

$(PROGRAM): $(OBJS) Main.o
	$(CC) $(CFLAGS) -o $@ $^

# default rule for compiling .cc to .o
%.o: %.cc
	$(CC) $(CFLAGS) -c $< -o $(patsubst %.cc,%.o,$<) ${INCLUDE}

## generate the prerequistes and append to the desired file
.prereq : $(OBJS:.o=.cc) $(wildcard *.h) Makefile
	rm -f .prereq
	$(CC) $(CCFLAGS) -MM $(OBJS:.o=.cc) >> ./.prereq 

     ## include the generated prerequisite file
     include .prereq

.PHONY: clean
clean:
	rm -rf *~ *.o $(TEST_SRC)/*.o *.gcov $(TEST_SRC)/*.gcov *.gcda $(TEST_SRC)/*.gcda *.gcno $(TEST_SRC)/*.gcno $(COVERAGE_RESULTS)  
	

.PHONY: clean-all
clean-all: clean
	rm -rf $(PROGRAM) $(PROGRAM_TEST)
	
run:
	$(PROGRAM)

memcheck: $(PROGRAM)
	valgrind --leak-check=yes $(PROGRAM)
	
$(PROGRAM_TEST): $(TEST_OBJS) $(OBJS)
	$(CC) $(CFLAGS) -o $@ $^ -lcppunit $(INCLUDE) 

test: $(PROGRAM_TEST)
	$(PROGRAM_TEST)
	
test-memcheck: $(PROGRAM_TEST)
	rm -f results
	valgrind --leak-check=yes $(PROGRAM_TEST) &> results
	more results

coverage: test	
	$(GCOV) *.cc 
	cd $(TEST_SRC)
	$(GCOV) *.cc
	cd ..
	grep -r "####" *.cc.gcov > $(COVERAGE_RESULTS)
	more $(COVERAGE_RESULTS)  
	