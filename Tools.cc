#include <string>
#include "Tools.h"
using namespace std;

//Creates a tool with the name=n
Tools::Tools(string n){
	name = n;
}

//Returns the name of the tool
string Tools::getName(){
	return name;
}
