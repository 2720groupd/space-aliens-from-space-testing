#include <algorithm>
#include <string>
#include <assert.h>
#include <cstdlib> //used for srand and rand
#include "Game.h"
#include "Scene.h"

using namespace std;

Game::Game(){
	user = new Player();
}

//Starts game
void Game::playGame()
{
  playScene(0);
}

void Game::playScene(int sceneNumber)
{
  switch (sceneNumber) {
  case 0:
    {
      MainMenu temp;
      break;
    }
  case 1:
    {
      Scene_1 temp;
      if (!temp.play(user)) 
	break; 
    }
  case 2:
    {
      Scene_2 temp;
      if (!temp.play(user))
	break;
    }
  case 3:
    {
      Scene_3 temp;
      if (!temp.play(user))
	break;
    }
  case 4:
    {
      Scene_4 temp;
      if (!temp.play(user))
	break;
    }
  case 5:
    {
      Scene_5 temp;
      if (!temp.play(user))
	break;
    }
  case 6:
    {
      Scene_6 temp;
      if (!temp.play(user))
	break;
    }
     case 7:
     {
	Scene_7 temp;
	if (temp.play(user) == false)//returns false. Decline to help alien.
        sceneNumber = 8;
	else //returns true. Agrees to help alien.
        sceneNumber = 10;
	  }
  default:
    break;
  }
    
    switch(sceneNumber)
    {
        case 8:
        {
            Scene_8 temp; //death not possible in this scene.
            temp.play(user);
        }
        case 9:
        {
            Scene_9 temp;
            if(!temp.play(user))
                break;
        }
        case 12:
        {
            Scene_12 temp;
            temp.play(user);
        }
        default:
	  break;
    }
    
    switch(sceneNumber)
    {
        case 10:
        {
            Scene_10 temp;
            if(!temp.play(user))
                break;
        }
        case 11:
        {
            Scene_11 temp;
            if(!temp.play(user))
                break;
        }
        case 12:
        {
            Scene_12 temp;
            if(!temp.play(user))
                break;
        }
        default:
            break;
    }
}


//Asks user for their choice and returns their choice
string Game::getUserChoice()
{
  
  string temp; //temp string for user choice                                                                                                                              
  
  bool validChoice=0; //indicates if player chose someething valid                                                                                                                                               
  
  //Universal functions user should always be able to access                                                                                                                                                    
  options.push_back("display stats"); //Will output hp and dmg                                                                                                                                                  
  options.push_back("list inventory"); //Will output list of inventory 
  options.push_back("quit game"); //Quits the game
  
  do {
    
    //Will output all possible commands if user types 'help'
    if (temp == "help")
      cout << "This game works on a verb / noun system." << endl << "Try entering a command with similar credentials to start the game (ex: 'new game' , 'look around')" << endl << "If you are still stuck type 'hint'" << endl;
    
    if (temp == "hint")
      for (unsigned int i=0; i<options.size(); i++)
	cout << "::" << options[i] << endl;
    
    //Get user input
    cout << ";;";
    getline(cin, temp);
    cout << endl << endl << endl;
	    
    //cast string to lower case                                                                                                                                                                                  
    transform(temp.begin(), temp.end(), temp.begin(), ::tolower);
	//generalize the verb
	temp = generalizedWord(temp);

    
    //Check is user input is a valid option
    for (unsigned int i=0; i<options.size(); i++)
      if (temp == options[i])
	validChoice = 1;
	if (temp != "help" && temp != "hint" && validChoice == 0)
		cout << "You tried to " << temp << " but slipped and fell." << endl << "Try typing 'help' for game mechanics description, or 'hint' for a list of possible choices." << endl;
      //cout << "INVALID_INPUT: Type 'help' for game mechanics description, or 'hint' for a list of possible choices." << endl;
    
    if (temp=="display stats"){
      cout << "Health" << setw(10) << setfill('.') << right << user->getHp()  << endl;
      cout << "Damage" << setw(10) << setfill('.') << right << user->getDmg() << endl;
      cout << endl;
      validChoice = 0;
    }
    
    if (temp=="list inventory"){
      user->getInventory();
      validChoice = 0;
    }

    if (temp=="quit game")
			assert(false);
    
  } while (!validChoice);
  
  cout << endl;
  return temp;
}

string Game::generalizedWord(string word)
{
	
	string generalized = "";

	for (unsigned int i = 0; i < word.length(); i++)
	{
		if (word[i] == ' ')
		{
//			cout << generalized  << "This is before the if statements" << endl;
			if (generalized == "use" || generalized == "eat"  || generalized == "throw" || generalized == "fire" || generalized == "lob" || generalized == " toss" || generalized == "push" || generalized == "press" || generalized == "activate")
			{
				generalized.clear();
				generalized = "use ";
			}

			else if (generalized == "explore" || generalized == "look" || generalized == "find" || generalized == "search" || generalized == "analyze" || generalized == "examine" || generalized == "seek" || generalized == "try" || generalized == "sticky" || generalized == "go" || generalized == "return")
			{
				generalized.clear();
				generalized = "explore ";
			}

			else if (generalized == "get" || generalized == "wake" || generalized == "stand")
			{
				cout << "\nget/wake/stand" << endl;
				generalized.clear();
				generalized = "get ";
			}

			else if (generalized == "attack" || generalized == "fight")
			{
				generalized.clear();
				generalized = "attack ";
			}

			else
				return word;

			for (unsigned int j = i + 1; j < word.length(); j++)
			{
				generalized = generalized + word[j];
			}

			
			return generalized; 
		}

		generalized = generalized + word[i];

	}
	cout << generalized << endl;
	return word;
}

//Died
void Game::gameOver(int sceneNumber, string reason){
  cout << "You have died on scene " << sceneNumber << ", " << reason << " HAHA!" <<  endl;
}

//Beat the game
void Game::isOver(){
  //TODO
}

//Fighting NPCs
//If returns 0 is user died, returns 1 if user won.
bool Game::battle(Creature *enemy){

  //random int seed
  srand(time(NULL));

  //If user wants to auto attack
  bool autoAttack=0;

  //while user is alive
  while (!(user->getHp()<=0)){

    cout << "\nYour  health = " << setw(10) << right << user->getHp()    << endl;
    cout << "Enemy health = " << setw(10) << right << enemy->getHp()  << endl;
		cout << endl;    
    
		//setting options
    options.clear();
    options.push_back("attack"); //[0]
    options.push_back("auto attack"); //[1]
    options.push_back("run"); //[2]
    string choice ="";  //setting options
    
    if (!autoAttack)
      choice = getUserChoice();
    if (choice == options[1])
      autoAttack=1;

    if (choice == options[0] || autoAttack){ //Attack
   
      //Random if user hits. 95% chance user hits
      int userHit = rand()%100;
      if (userHit<95){
	cout << "You attacked the enemy." << endl;
	cout << "Enemy health: " << enemy->getHp();
	user->attack(enemy);
	cout << " -> " << enemy->getHp() << endl;;
      }
      else
	cout << "Your attack missed!" << endl;
      cout << endl;
      
      //Random if enemy hits 80% chance
      if (enemy->getHp()!=0){
	int monsterHit = rand()%100;
      if (monsterHit<60){
	cout << "The monster attacks you!" << endl;
	user->takeDamage(enemy->getDmg());
	cout << "You took " << enemy->getDmg() << " damage!" << endl;
      }
      else 
	cout << "The monsters attack missed! " << endl;
      cout << endl;
      }
      else {
	cout << "You defeated the enemy!" << endl;
	delete enemy;
	return 1;
      }
    }
    
    if (choice == options[2]){ //run
      cout << "You are not allowed to run yet... coward." << endl;
    }
  }
  cout << "You died..." << endl;
  delete enemy;
  return 0;
}


